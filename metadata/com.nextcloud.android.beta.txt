Categories:Internet
License:GPL-2.0
Web Site:https://nextcloud.com
Source Code:https://github.com/nextcloud/android
Issue Tracker:https://github.com/nextcloud/android/issues
Changelog:https://github.com/nextcloud/android/blob/beta/CHANGELOG.md
Donate:https://www.bountysource.com/teams/nextcloud

Auto Name:Nextcloud beta
Summary:Synchronization client
Description:
A safe home for all your data. Access & share your files, calendars, contacts,
mail & more from any device, on your terms. This is a beta version of the
official Nextcloid app and includes brand-new, untested features which might
lead to instabilities and data loss. The app is designed for users willing to
test the new features and to report bugs if they occur. Do not use it for your
productive work!

The beta can be installed alongside the official Nextcloud app which is
available at F-Droid, too.
.

Repo Type:git
Repo:https://github.com/nextcloud/android.git

Build:20160612,20160612
    commit=beta-20160612
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160613,20160613
    commit=beta-20160613
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160617,20160617
    commit=beta-20160617
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160620,20160620
    commit=beta-20160620
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160621,20160621
    commit=beta-20160621
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160728,20160728
    commit=beta-20160728
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160730,20160730
    disable=java build fails
    commit=beta-20160730
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160731,20160731
    disable=java build fails
    commit=beta-20160731
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160802,20160802
    commit=beta-20160802
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160808,20160808
    commit=beta-20160808
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160809,20160809
    commit=beta-20160809
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160903,20160903
    commit=beta-20160903
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160907,20160907
    commit=beta-20160907
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20160922,20160922
    commit=beta-20160922
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161006,20161006
    commit=beta-20161006
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161012,20161012
    commit=beta-20161012
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161017,20161017
    commit=beta-20161017
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161019,20161019
    commit=beta-20161019
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161026,20161026
    commit=beta-20161026
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161027,20161027
    commit=beta-20161027
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161102,20161102
    commit=beta-20161102
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161107,20161107
    commit=beta-20161107
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161108,20161108
    commit=beta-20161108
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161109,20161109
    commit=beta-20161109
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161111,20161111
    commit=beta-20161111
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161123,20161123
    commit=beta-20161123
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161125,20161125
    commit=beta-20161125
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161129,20161129
    commit=beta-20161129
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161215,20161215
    commit=beta-20161215
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161222,20161222
    commit=beta-20161222
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161224,20161224
    commit=beta-20161224
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161229,20161229
    commit=beta-20161229
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20161230,20161230
    commit=beta-20161230
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170101,20170101
    commit=beta-20170101
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170106,20170106
    commit=beta-20170106
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170109,20170109
    commit=beta-20170109
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170111,20170111
    commit=beta-20170111
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170117,20170117
    commit=beta-20170117
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170122,20170122
    commit=beta-20170122
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170130,20170130
    commit=beta-20170130
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170202,20170202
    commit=beta-20170202
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170204,20170204
    commit=beta-20170204
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170205,20170205
    commit=beta-20170205
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170206,20170206
    commit=beta-20170206
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170207,20170207
    commit=beta-20170207
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170208,20170208
    commit=beta-20170208
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170209,20170209
    commit=beta-20170209
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170211,20170211
    commit=beta-20170211
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170213,20170213
    commit=beta-20170213
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170214,20170214
    commit=beta-20170214
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170217,20170217
    commit=beta-20170217
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170218,20170218
    commit=beta-20170218
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170219,20170219
    commit=beta-20170219
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170220,20170220
    commit=beta-20170220
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170221,20170221
    commit=beta-20170221
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170222,20170222
    commit=beta-20170222
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170223,20170223
    commit=beta-20170223
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170224,20170224
    disable=wrongly-tagged
    commit=beta-20170224
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170301,20170301
    disable=non-free google repo
    commit=beta-20170301
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170302,20170302
    disable=non-free google repo
    commit=beta-20170302
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170305,20170305
    disable=non-free google repo
    commit=beta-20170305
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170306,20170306
    disable=non-free google repo
    commit=beta-20170306
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:20170308,20170308
    commit=beta-20170309
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Auto Update Mode:Version beta-%c
Update Check Mode:Tags ^beta
Current Version:20170309
Current Version Code:20170309
